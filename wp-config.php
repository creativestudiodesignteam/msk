<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'devdesig_msk' );

/** MySQL database username */
define( 'DB_USER', 'devdesig_msk' );

/** MySQL database password */
define( 'DB_PASSWORD', 'q?C)xC4^q2jY' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'HZxd3$&e3rFc(:5`bI|nmo-w60h+[|(f%z@|}8~BCf$+wa+8<G* b`_C]ur{n|`R');
define('SECURE_AUTH_KEY',  'w-|1X _JBDj<0C^PZk%R^iH#!ZO9R*pCB%(3bj?hk|o3YYDy.yZnB5`)7GP+I5;:');
define('LOGGED_IN_KEY',    ',G^Hl|d!w/CL%p8kY2t0<m3P_Xh5^J||c?fb24< ~*GRC}|L0Om~|ZG]resmoV2l');
define('NONCE_KEY',        '2HXivqZ9nG8k*P2|ld!zU>WN96y5>=<5xtLS(XX<NhaIG-XaFwV(zOg+:A$MVZVO');
define('AUTH_SALT',        '@O{lYWFj2 1$gl+1.MUd]bNp*VL*]L|byP&#UmOnLgu-!+U+@II~+jlEx*61Zl2~');
define('SECURE_AUTH_SALT', 'KEApd|iZwby*Ag!xf8yP~Z)UAI{WNm$[8af)Z%5@x8.]HPCrP}JyZ>UVM9X|q`I(');
define('LOGGED_IN_SALT',   '@eFQ?k}WZM5Y8+b90wbn}]H3Giga+b>]YF(-|I9iYgn_K_(]~%]eRTH>~#-;{@`&');
define('NONCE_SALT',       'KB|J%w:Owc%iLln2++vdKXzC5H+L[{|>y+epPWpa:A<}+@NP@]S({P&YD{-2s=S1');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
