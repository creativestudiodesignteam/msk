=== Download Button for Elementor ===
Contributors: clicklabs® Medienagentur
Tags: elementor, elementor download button, elementor button, elementor addons, elementor download button widget
Requires at least: 4.6
Tested up to: 5.2.3
Stable tag: 1.0.0
Requires PHP: 5.6
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

A simple download button with the ability to choose a file from the media library.

== Description ==

<strong>Download Button for Elementor</strong>

Download Button for Elementor is a customized widget for Elementor Page Builder. 
Just drag and drop the widget to your page and select a file from the WordPress media library to download.
Without this plugin you would have to exit Elementor and copy the url for the file to download.

== Installation ==

1. Install the plugin through the WordPress plugins screen or upload the plugin files to the ‘/wp-content/plugins/plugin-name’ directory.
2. Activate the plugin through the 'Plugins' screen in WordPress

== Frequently Asked Questions ==

= Can I use Download Button for Elementor without Elementor? =

No.
This Plugin is an extension for Elementor Page Builder and it needs to be activated.

== Changelog ==

= 1.0 =
* Initial release.