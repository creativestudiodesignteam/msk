jQuery( window ).on( 'elementor:init', function() {

  var ControlMultipleBaseItemView = elementor.modules.controls.BaseData;

  ControlFileItemView = elementor.modules.controls.BaseData.extend({
    ui: function ui() {
      var ui = ControlMultipleBaseItemView.prototype.ui.apply(this, arguments);
      ui.controlMedia = '.elementor-control-media';
      ui.mediaImage = '.elementor-control-media__preview';
      ui.mediaVideo = '.elementor-control-media-video';
      ui.frameOpeners = '.elementor-control-preview-area';
      ui.removeButton = '.elementor-control-media__remove';
      return ui;
    },
    events: function events() {
      return _.extend(ControlMultipleBaseItemView.prototype.events.apply(this, arguments), {
        'click @ui.frameOpeners': 'openFrame',
        'click @ui.removeButton': 'deleteImage'
      });
    },
    getMediaType: function getMediaType() {
      return this.model.get('media_type');
    },
    applySavedValue: function applySavedValue() {
      var url = this.elementSettingsModel.get(this.model.get('name'))['url'];

      if(url.includes('.pdf')){
        pdfthumb = url.replace('.pdf','-pdf.jpg');
        this.ui.mediaImage.css('background-image', url ? 'url(' + pdfthumb + ')' : '');
      } else {
        this.ui.mediaImage.css('background-image', url ? 'url(' + url + ')' : '');
      }

      this.ui.controlMedia.toggleClass('elementor-media-empty', !url);
    },
    openFrame: function openFrame() {
      if (!this.frame) {
        this.initFrame();
      }
  
      this.frame.open();
      var selectedId = this.getControlValue('id');
  
      if (!selectedId) {
        return;
      }
  
      var selection = this.frame.state().get('selection');
      selection.add(wp.media.attachment(selectedId));
    },
    deleteImage: function deleteImage(event) {
      event.stopPropagation();
      this.setValue({
        url: '',
        id: ''
      });
      this.applySavedValue();
    },
  
    /**
     * Create a media modal select frame, and store it so the instance can be reused when needed.
     */
    initFrame: function initFrame() {
      // Set current doc id to attach uploaded images.
      wp.media.view.settings.post.id = elementor.config.document.id;
      this.frame = wp.media({
        button: {
          text: elementor.translate('insert_media')
        },
        states: [new wp.media.controller.Library({
          title: elementor.translate('insert_media'),
          library: wp.media.query({
            type: this.getMediaType()
          }),
          multiple: false,
          date: false
        })]
      }); // When a file is selected, run a callback.
  
      this.frame.on('insert select', this.select.bind(this));
    },
  
    /**
     * Callback handler for when an attachment is selected in the media modal.
     * Gets the selected image information, and sets it within the control.
     */
    select: function select() {
      this.trigger('before:select'); // Get the attachment from the modal frame.
  
      var attachment = this.frame.state().get('selection').first().toJSON();
  
      if (attachment.url) {
        this.setValue({
          url: attachment.url,
          id: attachment.id
        });
        this.applySavedValue();
      }
  
      this.trigger('after:select');
    },
    onBeforeDestroy: function onBeforeDestroy() {
      this.$el.remove();
    }
  });
  elementor.addControlView( 'file', ControlFileItemView );

} );