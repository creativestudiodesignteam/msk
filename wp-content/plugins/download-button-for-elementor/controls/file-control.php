<?php

use Elementor\Modules\DynamicTags\Module as TagsModule;


/**
 * Elementor file control.
 *
 * A control for displaying a dialog with the ability to add files.
 *
 * @since 1.0.0
 */
class File_Control extends \Elementor\Control_Base_Multiple {

	/**
	 * Get file control type.
	 *
	 * Retrieve the control type, in this case `file`.
	 *
	 * @since 1.0.0
	 * @access public
	 *
	 * @return string Control type.
	 */
	public function get_type() {
		return 'file';
	}

	/**
	 * Enqueue file control scripts and styles.
	 *
	 * Used to register and enqueue custom scripts and styles used by the file one
	 * area control.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function enqueue() {

		global $wp_version;

		$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
		wp_enqueue_media();

		wp_enqueue_style(
			'media',
			admin_url( '/css/media' . $suffix . '.css' ),
			[],
			$wp_version
		);

		wp_register_script(
			'image-edit',
			'/wp-admin/js/image-edit' . $suffix . '.js',
			[
				'jquery',
				'json2',
				'imgareaselect',
			],
			$wp_version,
			true
		);

		wp_enqueue_script( 'image-edit' );

		wp_register_script( 'file-control', plugin_dir_url(__DIR__).'/js/file-control.js', [ 'jquery' ], '1.0.0' );
		wp_enqueue_script( 'file-control' );
	}

	/**
	 * Get file control default settings.
	 *
	 * Retrieve the default settings of the file control. Used to return
	 * the default settings while initializing the file control.
	 *
	 * @since 1.0.0
	 * @access protected
	 *
	 * @return array Control default settings.
	 */
	protected function get_default_settings() {
		return [
			'label_block' => true
		];
	}

	/**
	 * Render file control output in the editor.
	 *
	 * Used to generate the control HTML in the editor using Underscore JS
	 * template. The variables for the class are available using `data` JS
	 * object.
	 *
	 * @since 1.0.0
	 * @access public
	 */
	public function content_template() {
		?>
		<div class="elementor-control-field elementor-control-media">
			<label class="elementor-control-title">{{{ data.label }}}</label>
			<div class="elementor-control-input-wrapper elementor-aspect-ratio-219">
				<div class="elementor-control-media__content elementor-control-tag-area elementor-control-preview-area elementor-fit-aspect-ratio">
					<div class="elementor-control-media-upload-button elementor-fit-aspect-ratio">
						<i class="eicon-plus-circle" aria-hidden="true"></i>
					</div>
					<div class="elementor-control-media-area elementor-fit-aspect-ratio">
						<div class="elementor-control-media__remove" title="<?php echo __( 'Remove', 'elementor' ); ?>">
							<i class="eicon-trash"></i>
						</div>
						<div class="elementor-control-media__preview elementor-fit-aspect-ratio"></div>
					</div>
					<div class="elementor-control-media__tools">
						<div class="elementor-control-media__tool elementor-control-media__replace"><?php echo __( 'Datei wählen', 'elementor' ); ?></div>
					</div>
				</div>
			</div>
			<# if ( data.description ) { #>
			<div class="elementor-control-field-description">{{{ data.description }}}</div>
			<# } #>
			<input type="hidden" data-setting="{{ data.name }}"/>
		</div>
		<?php
	}	

}